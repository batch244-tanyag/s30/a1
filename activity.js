/*
1. Create an activity.js file on where to write and save the solution for the activity.


2. Use the count operator to count the total number of fruits on sale.
*/

	db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "onSaleFruits"}
	]);



/*
3. Use the count operator to count the total number of fruits with stock more than 20.
*/
	db.fruits.aggregate([
	{$match: {stock: {$gt: 20}}},
	{$count: "Fruits with more than 20"}
	]);



/*
4. Use the average operator to get the average price of fruits onSale per supplier.
*/

	db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", onSaleAvgpricePerSupplier:{$avg: "$price"}}}
	]);
/*
5. Use the max operator to get the highest price of a fruit per supplier.
*/

	db.fruits.aggregate([
	{$group: {_id: "$supplierId", maxpricePerSupplier:{$max: "$price"}}}
	]);


/*
6. Use the min operator to get the lowest price of a fruit per supplier.
*/
	db.fruits.aggregate([
	{$group: {_id: "$supplierId", minpricePerSupplier:{$min: "$price"}}}
	]);


/*
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.

*/